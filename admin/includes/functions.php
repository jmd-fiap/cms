<?php

function inserirCategorias(){
  global $connection;
  if(isset($_POST['enviar'])){
      $cat_nome = $_POST['cat_nome'];
      if($cat_nome == ""){
              echo "Insira uma categoria";
          }
          else {
              $query = "INSERT INTO categorias(cat_nome) VALUE('$cat_nome')";
              $resultado = mysqli_query($connection, $query);

              if(!$resultado){
                        die("Não deu certo, porque: ") . mysqli_error($connection);
                    }
                            else {
                                echo "Categoria adicionada com sucesso";
                            }
        }
    }// fecha if
}

function mostraDadosCategoria(){
  global $connection;
  $query = "SELECT * from categorias";
  $select_categorias = mysqli_query($connection, $query);

      while($row = mysqli_fetch_assoc($select_categorias)){
        $cat_id = $row['cat_id'];
        $cat_nome = $row['cat_nome'];

        echo "<tr>";
        echo "<td>" . $cat_id . "</td>";
        echo "<td>" . $cat_nome . "</td>";
        echo "<td><a href='categorias.php?delete={$cat_id}'>Apagar</a></td>";
        echo "<td><a href='categorias.php?edit={$cat_id}'>Editar</a></td>";
        echo "</tr>";
      }

  if (isset($_GET['delete'])){
    $apaga_cat_id = $_GET['delete'];
    $query = "DELETE FROM categorias WHERE cat_id = {$apaga_cat_id}";
    $apagandoId = mysqli_query($connection, $query);
    header("Location: categorias.php");
  }


}

function listaPosts(){
  global $connection;
  $query = "SELECT * from posts";
  $select_posts = mysqli_query($connection, $query);

      while($row = mysqli_fetch_assoc($select_posts)){
        $post_id = $row['post_id'];
        $post_autor = $row['post_autor'];
        $post_nome = $row['post_nome'];
        $post_status = $row['post_status'];
        $post_imagem = $row['post_imagem'];
        //$post_data = $row['post_data'];
        $post_data = date('d-m-Y', strtotime($row['post_data']));
        $post_tags = $row['post_tags'];
        $post_conteudo = $row['post_conteudo'];

        echo "<tr>";
        echo "<td>" . $post_id . "</td>";
        echo "<td>" . $post_autor . "</td>";
        echo "<td>" . $post_nome . "</td>";
        echo "<td>" . $post_status . "</td>";
        echo '<td>' . '<img class="img-responsive" src=' . '../images/' . $post_imagem . '>' . "</td>";
        echo "<td>" . $post_data . "</td>";
        echo "<td>" . $post_tags . "</td>";
        echo "<td>" . $post_conteudo . "</td>";
        echo "<td><a href='categorias.php?delete={$post_id}'>Apagar</a></td>";
        echo "<td><a href='categorias.php?edit={$post_id}'>Editar</a></td>";
        echo "</tr>";

      }

      if (isset($_GET['delete'])){
        $apaga_post_id = $_GET['delete'];
        $query = "DELETE FROM posts WHERE post_id = {$apaga_post_id}";
        $apagandoId = mysqli_query($connection, $query);
        header("Location: post.php");
      }
}


?>
